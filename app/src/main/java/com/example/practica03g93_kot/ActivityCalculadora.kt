package com.example.practica03g93_kot

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class ActivityCalculadora : AppCompatActivity() {
    private lateinit var btnSumar: Button
    private lateinit var btnRestar: Button
    private lateinit var btnMultiplicar: Button
    private lateinit var btnDividir: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnRegresar: Button
    private lateinit var txtNumero1: EditText
    private lateinit var txtNumero2: EditText
    private lateinit var lblResultado: TextView
    private val calculadora = Calculadora(0.0f, 0.0f)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculadora)

        iniciarComponentes()

        btnSumar.setOnClickListener {
            btnSumar()
        }

        btnRestar.setOnClickListener {
            btnRestar()
        }

        btnDividir.setOnClickListener {
            btnDividir()
        }

        btnMultiplicar.setOnClickListener {
            btnMultiplicar()
        }

        btnLimpiar.setOnClickListener {
            limpiar()
        }

        btnRegresar.setOnClickListener {
            regresar()
        }
    }

    private fun iniciarComponentes() {
        btnSumar = findViewById(R.id.btnSumar)
        btnRestar = findViewById(R.id.btnRestar)
        btnMultiplicar = findViewById(R.id.btnMultiplicar)
        btnDividir = findViewById(R.id.btnDividir)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)
        txtNumero1 = findViewById(R.id.txtNumero1)
        txtNumero2 = findViewById(R.id.txtNumero2)
        lblResultado = findViewById(R.id.lblResultado)
    }

    private fun limpiar() {
        txtNumero1.setText("")
        txtNumero2.setText("")
        lblResultado.text = ""
    }

    private fun regresar() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Calculadora")
        confirmar.setMessage("¿Seguro de querer regresar?")

        confirmar.setPositiveButton("Confirmar") { dialog, which ->
            val con2 = Intent(this@ActivityCalculadora, MainActivity::class.java)
            startActivity(con2)
        }
        confirmar.show()
    }

    private fun btnSumar() {
        val valor1T = txtNumero1.text.toString()
        val valor2T = txtNumero2.text.toString()

        if (!valor1T.isEmpty() && !valor2T.isEmpty()) {
            calculadora.numero1 = valor1T.toFloat()
            calculadora.numero2 = valor2T.toFloat()
            lblResultado.text = calculadora.suma().toString()
        } else {
            Toast.makeText(this, "Capture ambos números", Toast.LENGTH_SHORT).show()
        }
    }

    private fun btnRestar() {
        val valor1T = txtNumero1.text.toString()
        val valor2T = txtNumero2.text.toString()

        if (!valor1T.isEmpty() && !valor2T.isEmpty()) {
            calculadora.numero1 = valor1T.toFloat()
            calculadora.numero2 = valor2T.toFloat()
            lblResultado.text = calculadora.resta().toString()
        } else {
            Toast.makeText(this, "Capture ambos números", Toast.LENGTH_SHORT).show()
        }
    }

    private fun btnMultiplicar() {
        val valor1T = txtNumero1.text.toString()
        val valor2T = txtNumero2.text.toString()

        if (!valor1T.isEmpty() && !valor2T.isEmpty()) {
            calculadora.numero1 = valor1T.toFloat()
            calculadora.numero2 = valor2T.toFloat()
            lblResultado.text = calculadora.multiplicacion().toString()
        } else {
            Toast.makeText(this, "Capture ambos números", Toast.LENGTH_SHORT).show()
        }
    }

    private fun btnDividir() {
        val valor1T = txtNumero1.text.toString()
        val valor2T = txtNumero2.text.toString()

        if (!valor1T.isEmpty() && !valor2T.isEmpty()) {
            calculadora.numero1 = valor1T.toFloat()
            calculadora.numero2 = valor2T.toFloat()
            lblResultado.text = calculadora.division().toString()
        } else {
            Toast.makeText(this, "Capture ambos números", Toast.LENGTH_SHORT).show()
        }
    }
}
