package com.example.practica03g93_kot

import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var btnCerrar: Button
    private lateinit var txtUsuario: EditText
    private lateinit var txtContrasena: EditText
    private lateinit var btnIngresar: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        iniciarComponentes()

        btnCerrar.setOnClickListener {
            cerrar()
        }

        btnIngresar.setOnClickListener {
            ingresar()
        }
    }

    private fun iniciarComponentes() {
        btnIngresar = findViewById(R.id.btnIngresar)
        btnCerrar = findViewById(R.id.btnCerrar)
        txtContrasena = findViewById(R.id.txtContrasena)
        txtUsuario = findViewById(R.id.txtUsuario)
    }

    private fun ingresar() {
        val strUsuario: String = getString(R.string.usuario)
        val strContra: String = getString(R.string.contrasena)

        if (txtUsuario.text.toString() == strUsuario && txtContrasena.text.toString() == strContra) {
            val con = Bundle()
            con.putString("usuario", strUsuario)

            val con2 = Intent(this@MainActivity, ActivityCalculadora::class.java)
            con2.putExtras(con)
            startActivity(con2)
        } else {
            Toast.makeText(applicationContext, "Usuario o contraseña no válidos", Toast.LENGTH_LONG).show()
        }
    }

    private fun cerrar() {
        finishAndRemoveTask()
    }
}