package com.example.practica03g93_kot

class Calculadora {
    var numero1: Float = 0.0f
    var numero2: Float = 0.0f

    constructor(numero1: Float, numero2: Float) {
        this.numero1 = numero1
        this.numero2 = numero2
    }

    fun suma(): Float {
        return numero1 + numero2
    }

    fun resta(): Float {
        return numero1 - numero2
    }

    fun multiplicacion(): Float {
        return numero1 * numero2
    }

    fun division(): Float {
        var total = 0.0f
        if (numero2 != 0.0f) {
            total = numero1 / numero2
        }
        return total
    }
}